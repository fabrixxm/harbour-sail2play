<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>AddInstance</name>
    <message>
        <source>Instance host name</source>
        <translation>Nombre de anfitrión</translation>
    </message>
    <message>
        <source>Instance name</source>
        <translation>Nombre de instancia</translation>
    </message>
</context>
<context>
    <name>Bookmarks</name>
    <message>
        <source>Bookmarks</source>
        <translation>Marcadores</translation>
    </message>
    <message>
        <source>Browse instances</source>
        <translation>Buscar instancias</translation>
    </message>
    <message>
        <source>Open from clipboard</source>
        <translation>Abrir portapapeles</translation>
    </message>
    <message>
        <source>Remove from bookmarks</source>
        <translation>Quitar marcadores</translation>
    </message>
    <message>
        <source>Add instance</source>
        <translation>Agregar instancia</translation>
    </message>
    <message>
        <source>Removing bookmark</source>
        <translation>Quitando marcador</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Ajustes</translation>
    </message>
</context>
<context>
    <name>InstancesList</name>
    <message>
        <source>No instances found</source>
        <translation>No hay instancias</translation>
    </message>
    <message>
        <source>That&apos;s odd.</source>
        <translation>Error</translation>
    </message>
    <message>
        <source>PeerTube Instances</source>
        <translation>Instancia de Peertube</translation>
    </message>
    <message>
        <source>Add to bookmarks</source>
        <translation>Agregar a marcadores</translation>
    </message>
    <message>
        <source>Remove from bookmarks</source>
        <translation>Quitar marcadores</translation>
    </message>
    <message>
        <source>Removing bookmark</source>
        <translation>Quitando marcador</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>Buscar</translation>
    </message>
</context>
<context>
    <name>Preferences</name>
    <message>
        <source>Video player</source>
        <translation>Reproductor  de video</translation>
    </message>
    <message>
        <source>Preferred video quality</source>
        <translation>Calidad de video preferida</translation>
    </message>
    <message>
        <source>Autoplay video</source>
        <translation>Autoreproducir video</translation>
    </message>
    <message>
        <source>Bookmarks</source>
        <translation>Marcadores</translation>
    </message>
    <message>
        <source>Reset bookmarks</source>
        <translation>Reiniciar marcadores</translation>
    </message>
    <message>
        <source>Preferred video download quality</source>
        <translation>Calidad de video a bajar</translation>
    </message>
    <message>
        <source>Contents</source>
        <translation>Contenidos</translation>
    </message>
    <message>
        <source>Show NSWF in results</source>
        <translation>mostrar NSWF a resultados</translation>
    </message>
    <message>
        <source>Show Only NSWF</source>
        <translation>Sólo mostrar NSWF</translation>
    </message>
</context>
<context>
    <name>SearchPage</name>
    <message>
        <source>Search</source>
        <translation>Buscar</translation>
    </message>
    <message>
        <source>No items</source>
        <translation>No hay elementos</translation>
    </message>
</context>
<context>
    <name>VideoDetails</name>
    <message>
        <source>Play</source>
        <translation>Reproducir</translation>
    </message>
    <message>
        <source>Open in browser</source>
        <translation>Abrir en navegador</translation>
    </message>
    <message>
        <source>Copy URL to clipboard</source>
        <translation>Copiar a portapapeles</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Ajustes</translation>
    </message>
    <message>
        <source>Download video</source>
        <translation>Bajar video</translation>
    </message>
</context>
<context>
    <name>VideoList</name>
    <message>
        <source>Search</source>
        <translation>Buscar</translation>
    </message>
    <message>
        <source>Trending</source>
        <translation>Tendencias</translation>
    </message>
    <message>
        <source>No items</source>
        <translation>No hay elementos</translation>
    </message>
    <message>
        <source>Try to select another instance</source>
        <translation>Intenta selecionando otra instancia</translation>
    </message>
    <message>
        <source>Recent</source>
        <translation>Reciente</translation>
    </message>
    <message>
        <source>local</source>
        <translation>Local</translation>
    </message>
    <message>
        <source>Show all</source>
        <translation>Mostrar todo</translation>
    </message>
    <message>
        <source>Show local</source>
        <translation>Mostrar local</translation>
    </message>
</context>
<context>
    <name>VideoPlayer</name>
    <message>
        <source>Error:</source>
        <translation>Error:</translation>
    </message>
    <message>
        <source>Video resolution</source>
        <translation>Resolución de video</translation>
    </message>
</context>
</TS>
