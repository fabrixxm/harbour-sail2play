<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it_IT">
<context>
    <name>AddInstance</name>
    <message>
        <location filename="../qml/pages/AddInstance.qml" line="13"/>
        <location filename="../qml/pages/AddInstance.qml" line="14"/>
        <source>Instance host name</source>
        <translation>Nome host istanza</translation>
    </message>
    <message>
        <location filename="../qml/pages/AddInstance.qml" line="20"/>
        <location filename="../qml/pages/AddInstance.qml" line="21"/>
        <source>Instance name</source>
        <translation>Nome istanza</translation>
    </message>
</context>
<context>
    <name>Bookmarks</name>
    <message>
        <location filename="../qml/pages/Bookmarks.qml" line="40"/>
        <source>Settings</source>
        <translation>Impostazioni</translation>
    </message>
    <message>
        <location filename="../qml/pages/Bookmarks.qml" line="45"/>
        <source>Add instance</source>
        <translation>Aggiungi istanza</translation>
    </message>
    <message>
        <location filename="../qml/pages/Bookmarks.qml" line="56"/>
        <source>Browse instances</source>
        <translation>Scopri istanze</translation>
    </message>
    <message>
        <location filename="../qml/pages/Bookmarks.qml" line="60"/>
        <source>Open from clipboard</source>
        <translation>Apri dagli appunti</translation>
    </message>
    <message>
        <location filename="../qml/pages/Bookmarks.qml" line="74"/>
        <source>Bookmarks</source>
        <translation>Preferiti</translation>
    </message>
    <message>
        <location filename="../qml/pages/Bookmarks.qml" line="82"/>
        <source>Remove from bookmarks</source>
        <translation>Rimuovi dai preferiti</translation>
    </message>
    <message>
        <location filename="../qml/pages/Bookmarks.qml" line="86"/>
        <source>Removing bookmark</source>
        <translation>Rimozione preferito</translation>
    </message>
</context>
<context>
    <name>InstancesList</name>
    <message>
        <location filename="../qml/pages/InstancesList.qml" line="40"/>
        <location filename="../qml/pages/InstancesList.qml" line="63"/>
        <source>Search</source>
        <translation>Cerca</translation>
    </message>
    <message>
        <location filename="../qml/pages/InstancesList.qml" line="56"/>
        <source>PeerTube Instances</source>
        <translation>Istanze PeerTube</translation>
    </message>
    <message>
        <location filename="../qml/pages/InstancesList.qml" line="90"/>
        <source>Add to bookmarks</source>
        <translation>Aggiungi ai preferiti</translation>
    </message>
    <message>
        <location filename="../qml/pages/InstancesList.qml" line="98"/>
        <source>Remove from bookmarks</source>
        <translation>Rimuovi dai preferiti</translation>
    </message>
    <message>
        <location filename="../qml/pages/InstancesList.qml" line="102"/>
        <source>Removing bookmark</source>
        <translation>Rimozione preferito</translation>
    </message>
    <message>
        <location filename="../qml/pages/InstancesList.qml" line="115"/>
        <source>No instances found</source>
        <translation>Nessuna istanza trovata</translation>
    </message>
    <message>
        <location filename="../qml/pages/InstancesList.qml" line="116"/>
        <source>That&apos;s odd.</source>
        <translation>Strano.</translation>
    </message>
</context>
<context>
    <name>Preferences</name>
    <message>
        <location filename="../qml/pages/Preferences.qml" line="28"/>
        <source>Video player</source>
        <translation>Lettore video</translation>
    </message>
    <message>
        <location filename="../qml/pages/Preferences.qml" line="37"/>
        <source>Preferred video quality</source>
        <translation>Qualità video preferita</translation>
    </message>
    <message>
        <location filename="../qml/pages/Preferences.qml" line="49"/>
        <source>Autoplay video</source>
        <translation>Avvia automaticamente il video</translation>
    </message>
    <message>
        <location filename="../qml/pages/Preferences.qml" line="55"/>
        <source>Preferred video download quality</source>
        <translation>Qualità video da scaricare</translation>
    </message>
    <message>
        <location filename="../qml/pages/Preferences.qml" line="67"/>
        <source>Bookmarks</source>
        <translation>Preferiti</translation>
    </message>
    <message>
        <location filename="../qml/pages/Preferences.qml" line="74"/>
        <source>Reset bookmarks</source>
        <translation>Reimposta preferiti</translation>
    </message>
    <message>
        <location filename="../qml/pages/Preferences.qml" line="97"/>
        <source>Contents</source>
        <translation>Contenuti</translation>
    </message>
    <message>
        <location filename="../qml/pages/Preferences.qml" line="105"/>
        <source>Show NSWF in results</source>
        <translation>Mostra risultati NSFW</translation>
    </message>
    <message>
        <location filename="../qml/pages/Preferences.qml" line="120"/>
        <source>Show Only NSWF</source>
        <translation>Mostra Solo NSFW</translation>
    </message>
</context>
<context>
    <name>SearchPage</name>
    <message>
        <location filename="../qml/pages/SearchPage.qml" line="32"/>
        <source>Search</source>
        <translation>Cerca</translation>
    </message>
    <message>
        <location filename="../qml/pages/SearchPage.qml" line="47"/>
        <source>No items</source>
        <translation>Nessun elemento</translation>
    </message>
</context>
<context>
    <name>VideoDetails</name>
    <message>
        <location filename="../qml/pages/VideoDetails.qml" line="36"/>
        <source>Settings</source>
        <translation>Impostazioni</translation>
    </message>
    <message>
        <location filename="../qml/pages/VideoDetails.qml" line="40"/>
        <source>Open in browser</source>
        <translation>Apri nel browser</translation>
    </message>
    <message>
        <location filename="../qml/pages/VideoDetails.qml" line="44"/>
        <source>Download video</source>
        <translation>Scarica video</translation>
    </message>
    <message>
        <location filename="../qml/pages/VideoDetails.qml" line="54"/>
        <source>Copy URL to clipboard</source>
        <translation>Copia URL negli appunti</translation>
    </message>
    <message>
        <location filename="../qml/pages/VideoDetails.qml" line="73"/>
        <source>Play</source>
        <translation>Avvia</translation>
    </message>
</context>
<context>
    <name>VideoList</name>
    <message>
        <location filename="../qml/pages/VideoList.qml" line="11"/>
        <location filename="../qml/pages/VideoList.qml" line="30"/>
        <location filename="../qml/pages/VideoList.qml" line="34"/>
        <source>Trending</source>
        <translation>Di tendenza</translation>
    </message>
    <message>
        <location filename="../qml/pages/VideoList.qml" line="39"/>
        <location filename="../qml/pages/VideoList.qml" line="43"/>
        <source>Recent</source>
        <translation>Recenti</translation>
    </message>
    <message>
        <location filename="../qml/pages/VideoList.qml" line="48"/>
        <source>Show all</source>
        <translation>Mostra tutti</translation>
    </message>
    <message>
        <location filename="../qml/pages/VideoList.qml" line="48"/>
        <source>Show local</source>
        <translation>Mostra locali</translation>
    </message>
    <message>
        <location filename="../qml/pages/VideoList.qml" line="56"/>
        <source>local</source>
        <translation>locali</translation>
    </message>
    <message>
        <location filename="../qml/pages/VideoList.qml" line="63"/>
        <source>Search</source>
        <translation>Cerca</translation>
    </message>
    <message>
        <location filename="../qml/pages/VideoList.qml" line="87"/>
        <source>No items</source>
        <translation>Nessun elemento</translation>
    </message>
    <message>
        <location filename="../qml/pages/VideoList.qml" line="88"/>
        <source>Try to select another instance</source>
        <translation>Prova a selezionare un&apos;altra istanza</translation>
    </message>
</context>
<context>
    <name>VideoPlayer</name>
    <message>
        <location filename="../qml/pages/VideoPlayer.qml" line="46"/>
        <source>Video resolution</source>
        <translation>Risoluzione video</translation>
    </message>
    <message>
        <location filename="../qml/pages/VideoPlayer.qml" line="266"/>
        <source>Error:</source>
        <translation>Errore:</translation>
    </message>
</context>
</TS>
