import QtQuick 2.0
import Nemo.DBus 2.0


// https://git.merproject.org/mer-core/transfer-engine/blob/master/dbus/org.nemo.transferengine.xml
// https://sailfishos.org/develop/docs/nemo-qml-plugin-dbus/qml-nemo-dbus-dbusinterface.html
DBusInterface {
    bus: DBus.SessionBus
    service: 'org.nemo.transferengine'
    iface: 'org.nemo.transferengine'
    path: '/org/nemo/transferengine'

    // https://git.merproject.org/mer-core/transfer-engine/blob/master/src/transferengine.cpp#L948
    function createDownload(displayName, filePath, expectedFileSize, cbk) {
        console.log("createDownload", displayName, filePath, expectedFileSize);
        var args = [
                    {'type':'s', 'value':displayName},
                    {'type':'s', 'value':'harbour-sail2play'}, // applicationIcon
                    {'type':'s', 'value':''}, // serviceIcon
                    {'type':'s', 'value':filePath},
                    {'type':'s', 'value':'video/mp4'}, // mimeType
                    {'type':'x', 'value': expectedFileSize},
                    {'type':'as', 'value':[ 'net.kirgroup.sail2play','/net/kirgroup/sail2play/transfer','net.kirgroup.sail2play.transfer']},
                    {'type':'s', 'value':'cancelTansfer'}, //cancelMethod
                    {'type':'s', 'value':''} //restartMethod
                ];
        typedCall('createDownload',args,
                  function (transferid) {
                      // This will be called when the result is available
                      console.log('Got transfer id: ' + transferid);
                      if (cbk) cbk(transferid);
                  }, _error);
    }

    function updateTransferProgress(transferid, progress) {
        console.log("updateTransferProgress",transferid,  progress);
        if (!transferid) return;
        var args = [
                    {'type':'i', 'value':transferid},
                    {'type':'d', 'value':progress}
                ];
        typedCall("updateTransferProgress",args, undefined, _error);
    }

    function startTransfer(transferid) {
        console.log("startTransfer", transferid);
        if (!transferid) return;
        typedCall("startTransfer", [{'type':'i', 'value':transferid}], undefined, _error);
    }

    function finishTransfer(transferid, status, reason) {
        console.log("finishTransfer", transferid, status, reason)
        if (!transferid) return;
        var args = [
                    {'type':'i', 'value':transferid},
                    {'type':'i', 'value':status},
                    {'type':'s', 'value':reason}
                ];
        typedCall("finishTransfer", args, undefined, _error);
    }

    function finishTransferSuccess(transferid) { finishTransfer(transferid, 4, ""); }
    function finishTransferCancelled(transferid) { finishTransfer(transferid, 3, ""); }
    function finishTransferInterrupted(transferid, reason) { finishTransfer(transferid, 5, reason); }

    function enableNotifications() {
        call("enableNotifications",[true], undefined, _error );
    }


    function _error(error) {
        console.log('call failed', error);
    }
}
