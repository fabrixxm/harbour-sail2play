import QtQuick 2.0
import Nemo.DBus 2.0


Item {
    signal cancel(int transferid)
    DBusAdaptor {
        id: dbus

        bus: DBus.SessionBus
        service: 'net.kirgroup.sail2play'
        iface: 'net.kirgroup.sail2play.transfer'
        path: '/net/kirgroup/sail2play/transfer'

        xml: '  <interface name="net.kirgroup.sail2play.transfer">\n' +
             '    <method name="cancelTansfer>\n' +
             '      <arg direction="in" type="i" name="transferId"/>\n'+
             '    </method>\n'+
             '  </interface>\n'

        function cancelTansfer(transferId) {
            console.log("cancel", transferId)
            cancel(transferId)
        }
    }
}
