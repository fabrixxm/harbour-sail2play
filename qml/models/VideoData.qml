import QtQuick 2.0
import "jsonpath.js" as JSONPath

JSONObjectData {
    property string uuid: ""
    source: uuid==="" ? "" : globals.apiurl+"/videos/"+uuid

    /*
    onJsonChanged: fetchExtraInfo()

    function get(attribute, url) {
        console.log(attribute, data[attribute]);
        var xhr = new XMLHttpRequest;
        xhr.open("GET", url);
        console.log(attribute, url);
        xhr.onreadystatechange = function() {
            console.log(xhr.readyState);
            if (xhr.readyState === XMLHttpRequest.DONE) {
                console.log(attribute, xhr.responseText);
                data[attribute] = JSON.parse(xhr.responseText)[attribute];
            }
        }
        xhr.send();
    }

    function fetchExtraInfo() {
        updateJSONModel();
        get('description', source + "/description");
    }
    */

    /**
     * return the file with a resolution equal or lower to the wanted
     *
     * if no resolution match, get the lowest possible.
     */
    function getFile(max_resolution) {

        if (data.files === undefined) return {};

        console.log("getFile", max_resolution);
        var r = JSONPath.jsonPath(data, "$.files[?(@.resolution.id <= "+max_resolution+")]");
        console.log("->", JSON.stringify(r));
        if (r===false) {
            console.log("no resolution found. get last");
            r = JSONPath.jsonPath(data, "$.files[-1:]");
            console.log("->", JSON.stringify(r));
        }

        return r[0];
    }
}
