import QtQuick 2.0

JSONListModel {
    property string sort: "-trending"
    property string filter: ""

    source: globals.apiurl + "/videos/?sort=" + sort + _getNSFWParam() + (filter == "" ? "" : ("&filter=" + filter))
    query: "$.data[*]"

    function _getNSFWParam() {
        if (conf.showNSFW) {
            if (conf.showOnlyNSFW) return "&nsfw=true";
            return "";
        }
        return "&nsfw=false";
    }


    function filterJson(jo){
        // filter out nsfw result
        // (should be filtered by source url, but we'll keep it to be compatibile with older api)s
        if (!conf.showNSFW && jo.nsfw) return false;
        return true;
    }

    function orderTrending() { start = 0; sort = "-trending";  }
    function orderRecent() { start = 0; sort = "-publishedAt"; }

    function filterAll() { start = 0; filter = ""; }
    function filterLocal() { start = 0; filter = "local"; }

}

/*  overview @ /overviews/videos */
