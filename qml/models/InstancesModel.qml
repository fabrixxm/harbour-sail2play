import QtQuick 2.0

JSONListModel {
    property string search: ""

    source: "https://instances.joinpeertube.org/api/v1/instances"
    query: "$.data[*]"

    paginationCount: 500;

    onSearchChanged: updateJSONModel(json)
    
    function filterJson(jo) {
        if (search === "") return true;
        search = search.toLowerCase();
        return (
            jo['name'].toLowerCase().match(search) !== null ||
            jo['host'].toLowerCase().match(search) !== null ||
            jo['shortDescription'].toLowerCase().match(search) !== null
        );
    }
    
}
