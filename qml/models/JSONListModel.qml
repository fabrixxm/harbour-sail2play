/* JSONListModel - a QML ListModel with JSON and JSONPath support
 *
 * Copyright (c) 2012 Romain Pokrzywka (KDAB) (romain@kdab.com)
 * Licensed under the MIT licence (http://opensource.org/licenses/mit-license.php)
 */

import QtQuick 2.0
import "jsonpath.js" as JSONPath

Item {
    property string source: ""
    property string json: ""
    property string query: ""

    property ListModel model : ListModel { id: jsonModel }
    property alias count: jsonModel.count

    property int paginationCount: globals.paginationCount
    property int total: 0
    property int start: 0

    property int status: -1

    onSourceChanged: {
        console.log("onSourceChanged", source);
        var theurl = theUrl();
        status = 0

        var xhr = new XMLHttpRequest;
        xhr.open("GET", theurl);
        xhr.onreadystatechange = function() {
            status = xhr.status
            if (xhr.readyState == XMLHttpRequest.DONE)
                json = xhr.responseText;
        }
        xhr.send();
    }

    onJsonChanged: updateJSONModel(json)
    onQueryChanged: updateJSONModel(json)

    function updateJSONModel(jsonString) {
        jsonModel.clear();
        extendJSONModel(jsonString);
    }

    function extendJSONModel(jsonString) {
        if ( jsonString === "" )
            return;

        var objectArray = parseJSONString(jsonString, query);
        for ( var key in objectArray ) {
            var jo = objectArray[key];
            if (filterJson(jo)) {
                jsonModel.append( jo );
            }
        }
    }

    // filter json object.
    // return true if object should be added in List, false if not.
    function filterJson(jo){ return true; }

    function parseJSONString(jsonString, jsonPathQuery) {
        var objectArray = JSON.parse(jsonString);
        setTotal(objectArray);
        if ( jsonPathQuery !== "" )
            objectArray = JSONPath.jsonPath(objectArray, jsonPathQuery);

        return objectArray;
    }

    function setTotal(objectArray) {
        var r = JSONPath.jsonPath(objectArray, "$.total");
        total = 0;
        if (r.length === 1 ) total = r[0];
    }

    function theUrl() {
        if (source == "") return "";
        var theurl = source;
        if (theurl.indexOf("?") < 0) {
            theurl = theurl + "?";
        } else {
            theurl = theurl + "&";
        }
        theurl = theurl + "count=" + paginationCount;
        theurl = theurl + "&start=" + start;
        console.log("the url:", theurl);
        return theurl;
    }

    function loadMore() {
        if (count>=total) return;

        start = start + paginationCount + 1;
        status = 0

        var xhr = new XMLHttpRequest;
        xhr.open("GET", theUrl());
        xhr.onreadystatechange = function() {
            status = xhr.status
            if (xhr.readyState == XMLHttpRequest.DONE) {
                var jsonString = xhr.responseText;
                extendJSONModel(jsonString);
            }
        }
        xhr.send();
    }

}
