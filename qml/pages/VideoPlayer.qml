import QtQuick 2.0
import Sailfish.Silica 1.0
import QtMultimedia 5.6
import org.nemomobile.mpris 1.0

import "../elements"
import "../utils.js" as Utils

/**
  Video Player Page

  this page MUST be loaded with video object in globals.currentVideo
*/
Page {
    id: page
    property bool firstPageChange: true

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All

    function onThisPage() {
        applicationWindow.cover.state = "videoplayer"
    }

    Component.onCompleted: {
        pageStack.onCurrentPageChanged.connect(function(){
            // set mpris on page change
            mprisPlayer.cover = globals.instanceurl + globals.currentVideo.data.previewPath
            mprisPlayer.album = globals.currentVideo.data.channel.displayName
            mprisPlayer.artist = globals.currentVideo.data.account.displayName
            mprisPlayer.song = globals.currentVideo.data.name

            if (firstPageChange) {
                if (conf.autoPlay) videoPlayer.playPause();
                firstPageChange = false;
                return;
            }
            if (videoPlayer) videoPlayer.pause();
        })
    }

    SilicaFlickable {
        anchors.fill:  parent

        PushUpMenu {
            MenuLabel { text: qsTr("Video resolution") }
            Repeater {
                model: globals.currentVideo.data.files
                MenuItem {
                    text: globals.currentVideo.data.files[index].resolution.label
                    onClicked: videoPlayer.currentFile = globals.currentVideo.data.files[index]
                    font.bold: videoPlayer.currentFile !== undefined ? videoPlayer.currentFile.resolution.id === globals.currentVideo.data.files[index].resolution.id : false
                }
            }
        }

        Video {
            property bool isPlaying: false
            property variant currentFile: undefined

            id: videoPlayer
            anchors.fill: parent;

            onPlaying: isPlaying = true
            onPaused: isPlaying = false

            onIsPlayingChanged: {
                // sreen blanking
                mce.preventScreenBlanking = isPlaying

                // mpris
                mprisPlayer.playbackStatus = isPlaying ? Mpris.Playing : Mpris.Paused;
            }
            source: ""

            onSourceChanged: console.log("video source:", source)

            onCurrentFileChanged: source = currentFile!==undefined ? currentFile.fileUrl : ""

            function proxiedUrl(url) {
                return "http://localhost:56740/" + url.replace("https://", "");
            }

            onStatusChanged: {
                console.log("onStatusChanged", status);
                if (status !== MediaPlayer.NoMedia) poster.visible = false;

            }

            onErrorChanged: {
                console.log("onErrorChanged", error, errorString);
                if (errorString.toLowerCase().indexOf("secure connection setup failed")>-1) {
                    videoPlayer.playPause();
                    source = proxiedUrl(currentFile.fileUrl);
                    if (conf.autoPlay) videoPlayer.playPause();
                }
            }

            function playPause() {
                if ( isPlaying ) {
                    pause();
                } else {
                    if (currentFile === undefined) currentFile = globals.currentVideo.getFile(conf.preferredQuality);
                    play();
                    page.state = "nocontrols"
                }
            }
        }

        Image {
            id: poster
            anchors.fill: parent
            source: globals.instanceurl + globals.currentVideo.data.previewPath
            fillMode: Image.PreserveAspectFit
        }

        MouseArea {
            anchors.fill: parent
            propagateComposedEvents: true
            onClicked: {
                page.state = (page.state === "nocontrols") ? "controls" : "nocontrols";
            }
        }

        PageHeader {
            id: pageHeader
            title: globals.currentVideo.data.name
            anchors {
                top: parent.top
                left: parent.left
                right: parent.righ
            }
        }

        BusyIndicator {
            anchors.centerIn: parent;
            size: BusyIndicatorSize.Large
            running: videoPlayer.status == MediaPlayer.Loading
        }



        Item {
            id: progressbar
            anchors {
                bottom: parent.bottom
                left: parent.left
                right: parent.right
            }

            height: playing.height + Theme.paddingSmall*2

            Rectangle {
                anchors.fill: parent
                color: Theme.highlightBackgroundColor
                opacity: Theme.highlightBackgroundOpacity
                visible: buffering.visible || playing.visible
            }

            ProgressBar {
                id: buffering
                indeterminate: true
                anchors {
                    left: parent.left
                    right: parent.right
                    bottom: parent.bottom
                    bottomMargin: Theme.paddingSmall + 20
                }
                visible: (videoPlayer.status == MediaPlayer.Buffering) || (videoPlayer.status == MediaPlayer.Loading)
            }
            Slider {
                id: playing
                anchors {
                    left: parent.left
                    right: parent.right
                    bottom: parent.bottom
                    bottomMargin: Theme.paddingSmall
                }
                minimumValue: 0;
                maximumValue: globals.currentVideo.data.duration * 1000
                value: videoPlayer.position
                valueText: Utils.s2str(videoPlayer.position / 1000)
                visible: videoPlayer.status !== MediaPlayer.NoMedia
                onValueChanged: if (down) videoPlayer.seek(value - videoPlayer.position)
            }
        }


        Item {
            id: playpausebtn
            anchors.centerIn: parent
            width: playpausebtninner.width
            height: playpausebtninner.height
            Rectangle {
                anchors.fill: parent
                radius: width/2
                color: "black"
                opacity: 0.3
            }

            IconButton {
                id: playpausebtninner
                anchors.centerIn: parent
                icon.source: videoPlayer.isPlaying ? "image://theme/icon-l-pause" : "image://theme/icon-l-play"
                onClicked: {
                    console.log("playpausebtn onClicked");
                    videoPlayer.playPause();
                }
            }
        }
    }


    states: [
        State {
            name: "controls"
            PropertyChanges { target: playpausebtn; opacity: 1.0; visible: true }
            PropertyChanges { target: progressbar; visible: true; anchors.bottomMargin: 0 }
            PropertyChanges { target: pageHeader; opacity: 1.0; visible: true }
        },

        State {
            name: "nocontrols"
            PropertyChanges { target: playpausebtn; opacity: 0.0; visible: false }
            PropertyChanges { target: progressbar;  visible: false; anchors.bottomMargin: -progressbar.height }
            PropertyChanges { target: pageHeader; opacity: 0.0;  visible: false }
        }
    ]

    transitions: [
        Transition {
            from: "controls"; to: "nocontrols"
            SequentialAnimation{
                NumberAnimation { properties: "opacity,anchors.bottomMargin"; duration: kStandardAnimationDuration }
                PropertyAnimation { properties: "visible"; duration: 0 }
            }
        },
        Transition {
            from: "nocontrols"; to: "controls"
            SequentialAnimation{
                PropertyAnimation { properties: "visible"; duration: 0 }
                NumberAnimation { properties: "opacity,anchors.bottomMargin"; duration: kStandardAnimationDuration }
            }
        }
    ]

    state: "controls"

    Item {
        id: errorMessage
        anchors.centerIn: parent
        width: parent.width
        height: errorLabel.height + (Theme.paddingMedium * 2)

        visible: videoPlayer.error !== MediaPlayer.NoError

        Rectangle {
            color: "#000000"
            anchors.fill: parent;
        }
        Label {
            id: errorLabel
            anchors.centerIn: parent
            width: parent.width
            wrapMode: Text.WordWrap
            text: qsTr("Error:") + videoPlayer.errorString
            color: "#FFFFFF"
            horizontalAlignment: Text.AlignHCenter
        }

    }


    // mpris interface
    MprisPlayer {
        id: mprisPlayer

        property string cover
        property string artist
        property string album
        property string song

        serviceName: "sail2play"

        // Mpris2 Root Interface
        identity: "Sail2Play"
        supportedUriSchemes: ["http", "https"]
        supportedMimeTypes: ["video/mp4"]

        // Mpris2 Player Interface
        canControl: true

        canGoNext: false
        canGoPrevious: false
        canPause: playbackStatus === Mpris.Playing
        canPlay: playbackStatus !== Mpris.Playing
        canSeek: false

        playbackStatus: Mpris.Stopped
        loopStatus: Mpris.None
        shuffle: false
        volume: 1

        onPauseRequested: { console.log("Pause requested"); videoPlayer.playPause() }
        onPlayRequested: { console.log("Play requested"); videoPlayer.playPause() }
        onPlayPauseRequested: { console.log("Play/Pause requested"); videoPlayer.playPause() }
        onStopRequested: { console.log("Stop requested"); videoPlayer.playPause() }
        onNextRequested: console.log("Next requested")
        onPreviousRequested: console.log("Previous requested")
        onSeekRequested: {
            console.log("Seeked requested with offset - " + offset + " microseconds")
            emitSeeked()
        }
        onSetPositionRequested: {
            console.log("Position requested to - " + position + " microseconds")
            emitSeeked()
        }
        onOpenUriRequested: console.log("Requested to open uri \"" + url + "\"")

        /*
        onLoopStatusRequested: {
            if (loopStatus == Mpris.None) {
                repeatSwitch.checked = false
            } else if (loopStatus == Mpris.Playlist) {
                repeatSwitch.checked = true
            }

        }
        onShuffleRequested: shuffleSwitch.checked = shuffle
        */

        onCoverChanged: {
            console.log("mpris cover changed:", cover)
            var metadata = mprisPlayer.metadata

            metadata[Mpris.metadataToString(Mpris.ArtUrl)] = cover // string url

            mprisPlayer.metadata = metadata
            _dbgobj(metadata);
        }


        onArtistChanged: {
            console.log("mpris artist changed:", artist)
            var metadata = mprisPlayer.metadata

            metadata[Mpris.metadataToString(Mpris.Artist)] = [artist] // List of strings

            mprisPlayer.metadata = metadata
            _dbgobj(metadata);
        }

        onAlbumChanged: {
            console.log("mpris album changed:", album)
            var metadata = mprisPlayer.metadata

            metadata[Mpris.metadataToString(Mpris.Album)] = album // String

            mprisPlayer.metadata = metadata
            _dbgobj(metadata);
        }

        onSongChanged: {
            console.log("mpris song changed:", song)
            var metadata = mprisPlayer.metadata

            metadata[Mpris.metadataToString(Mpris.Title)] = song // String

            mprisPlayer.metadata = metadata
            _dbgobj(metadata);
        }

    }

    function _dbgobj(obj) {
        /*console.log("-----v")
        for (var k in obj) {
            console.log(k, "=", obj[k]);
        }
        console.log("-----^")*/
    }

}
