import QtQuick 2.0
import Sailfish.Silica 1.0

import "../models"
import "../elements"
import "../utils.js" as Utils
Page {
    id: page
    property alias uuid: video.uuid

    allowedOrientations: Orientation.All

    function onThisPage() {
        applicationWindow.cover.state = "videodetais"
    }


    VideoData {
        id: video
        onDataChanged: {
            globals.currentVideo = video;
            if (data.files.length > 0) {
                pageStack.pushAttached(Qt.resolvedUrl("VideoPlayer.qml"))
            }
         }
    }

    SilicaFlickable {
        id: flickable
        anchors.fill: parent
        contentHeight: column.height


        PullDownMenu {
            MenuItem {
                text: qsTr("Settings")
                onClicked: pageStack.push(Qt.resolvedUrl("Preferences.qml"));
            }
            MenuItem {
                text: qsTr("Open in browser")
                onClicked: Qt.openUrlExternally(globals.instanceurl + "/videos/watch/" + video.uuid)
            }
            MenuItem {
                text: qsTr("Download video")
                enabled: video.data && data.files.length > 0
                onClicked: {
                    var currentFile = globals.currentVideo.getFile(conf.preferredDownloadQuality);
                    downloader.get(globals.currentVideo.data.name,
                                          currentFile.fileDownloadUrl,
                                          currentFile.size)
                }
            }
            MenuItem {
                text: qsTr("Copy URL to clipboard")
                onClicked: Clipboard.text = globals.instanceurl + "/videos/watch/" + video.uuid
            }

        }


        Column {
            id: column
            anchors {
                leftMargin: Theme.paddingMedium
                rightMargin: Theme.paddingMedium
                left: parent.left
                right: parent.right
            }

            spacing: Theme.paddingMedium

            PageHeader {
                title: qsTr("Play")
                visible: video.data.files.length>0
            }

            Image {
                source: globals.instanceurl + video.data.previewPath
                width: parent.width
                height: width * 9/16
            }
/*
            Row {
                spacing: 5
                layoutDirection: Qt.RightToLeft
                Repeater{
                    model: video.data.files
                    Rectangle {
                        color:"black"
                        radius: 5
                        width: resLabel.width + 10
                        height: resLabel.height + 10
                        Label {
                            id:resLabel
                            color: "#ffffff"
                            text: video.data.files[index].resolution.label
                            font.pixelSize: Theme.fontSizeTiny
                            horizontalAlignment: Text.AlignRight
                            verticalAlignment: Text.AlignTop
                            anchors {
                                top: parent.top
                                left: parent.left
                                topMargin: 5
                                leftMargin: 5
                            }
                        }
                    }
                }
            }
*/
            Label {
                text: video.data.name
                color: Theme.highlightColor
                wrapMode: Text.WordWrap
                width: parent.width
            }

            Separator { width: parent.width; color: Theme.primaryColor }

            Row {
                spacing: Theme.paddingLarge

                Row {
                    spacing: Theme.paddingMedium
                    Image { source: "image://theme/icon-s-duration?"+Theme.secondaryHighlightColor }
                    Label { text: Utils.s2str(video.data.duration); color: Theme.highlightColor}
                }

                Row {
                    spacing: Theme.paddingMedium
                    Image { source: "image://theme/icon-s-device-download?"+Theme.secondaryHighlightColor }
                    Label { text: video.data.views ; color: Theme.highlightColor}
                }

                Row {
                    spacing: Theme.paddingMedium
                    Image { source: "image://theme/icon-s-like?"+Theme.secondaryHighlightColor }
                    Label { text: video.data.likes; color: Theme.highlightColor }
                }
            }

            Row {
                spacing: Theme.paddingMedium
                Image { source: "image://theme/icon-s-cloud-upload?"+Theme.secondaryHighlightColor }
                Label { text: Utils.json2date(video.data.publishedAt).toLocaleString(); color: Theme.highlightColor }
            }

            Separator { width: parent.width; color: Theme.primaryColor }

            Row {
                spacing: Theme.paddingLarge

                Row {
                    spacing: Theme.paddingMedium
                    Image { source: "image://theme/icon-m-person?"+Theme.secondaryHighlightColor; height: Theme.iconSizeSmall; width: height; }
                    Label { text: video.data.account.displayName; color: Theme.highlightColor }
                }

                Row {
                    spacing: Theme.paddingMedium
                    Image { source: "image://theme/icon-s-group-chat?"+Theme.secondaryHighlightColor }
                    Label { text: video.data.channel.displayName; color: Theme.highlightColor }
                }
            }

            Separator { width: parent.width; color: Theme.primaryColor }

            Label {
                text: video.data.description
                wrapMode: Text.WordWrap
                width: parent.width
                color: Theme.highlightColor
            }

        }

        VerticalScrollDecorator {}
    }

    LoadingSpinner {
        model: video
    }
}
