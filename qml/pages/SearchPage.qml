import QtQuick 2.0
import Sailfish.Silica 1.0
import "../models"
import "../elements"

Page {
    id: page
    property string searchString: ""

    onSearchStringChanged: console.log("search string changed", searchString);

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All

    SearchModel {
        id: searchModel
        search: searchString
    }

    S2PListView {
        id: listView
        model: searchModel.model
        anchors.fill: parent

        onAtYEndChanged: {
            if (atYEnd) searchModel.loadMore();
        }

        header: SearchField {
            id: searchField
            width: parent.width
            placeholderText: qsTr("Search")
            onTextChanged: {
                if (text==="") searchString = text
            }

            EnterKey.enabled: text.length > 0
            EnterKey.onClicked: searchString = text
        }

        delegate: VideoItem {
            onClicked: pageStack.push(Qt.resolvedUrl("VideoDetails.qml"), {uuid:model.uuid})
        }

        ViewPlaceholder {
            enabled: listView.count == 0
            text: qsTr("No items")
            //hintText: "Pull down to add items"
        }

        VerticalScrollDecorator {}
    }

    LoadingSpinner {
        model: searchModel
    }
}
