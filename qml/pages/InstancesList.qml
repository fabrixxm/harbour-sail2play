import QtQuick 2.0
import Sailfish.Silica 1.0
import "../models"
import "../elements"

Page {
    id: page

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All

    property bool inSearch: false
    property variant bookmarked: []
    Component.onCompleted: {
        conf.onBookmarksChanged.connect(updateBookmarked);
        updateBookmarked();
    }

    function updateBookmarked(){
        console.log("updateing bookmarked");
        var blist = JSON.parse(conf.bookmarks);
        bookmarked = []
        for(var k in blist) {
            bookmarked.push(blist[k].host);
        }
        console.log(JSON.stringify(bookmarked));
    }



    InstancesModel {
        id: instancesModel
    }


    S2PListView {

        PullDownMenu {
            MenuItem {
                text: qsTr("Search")
                onClicked: {
                    inSearch = !inSearch;
                }
            }
        }

        id: listView
        model: instancesModel.model
        anchors.fill: parent

        header: Item {
            width: parent.width
            height: inSearch ? searchField.height : headerField.height
            PageHeader {
                id: headerField
                title: qsTr("PeerTube Instances")
                visible: !inSearch
            }
            SearchField {
                id: searchField
                visible: inSearch
                width: parent.width
                placeholderText: qsTr("Search")
                onTextChanged: {
                    if (text==="") instancesModel.search = ""
                }

                EnterKey.enabled: text.length > 0
                EnterKey.onClicked: instancesModel.search = text
            }
        }

        quickScroll: true

        onAtYEndChanged: {
            if (atYEnd) instancesModel.loadMore();
        }

        delegate: InstanceItem {
            id: delegate
            property bool isBookmarked: bookmarked.indexOf(model.host) >= 0
            onClicked: {
                globals.instanceurl = "https//" + model.host
                globals.instance = model.name

                pageStack.push(Qt.resolvedUrl("VideoList.qml"));
            }
            menu: ContextMenu {
                MenuItem {
                    text: qsTr("Add to bookmarks")
                    onClicked: {
                        delegate.isBookmarked = true;
                        conf.addBookmark({name:model.name, host:model.host})
                    }
                    visible: !delegate.isBookmarked
                }
                MenuItem {
                    text: qsTr("Remove from bookmarks")
                    RemorseItem { id: remorse }
                    function showRemorseItem() {
                        var idx = index
                        remorse.execute(delegate, qsTr("Removing bookmark"), function() {
                            delegate.isBookmarked = false;
                            conf.removeBookmark(idx);
                        })
                    }
                    onClicked: showRemorseItem()
                    visible: delegate.isBookmarked
                }
            }
        }

        ViewPlaceholder {
            enabled: listView.count == 0 && instancesModel.status > 0
            text: qsTr("No instances found")
            hintText: qsTr("That's odd.")
        }

        VerticalScrollDecorator { flickable: listView }
    }

    LoadingSpinner {
        model: instancesModel
    }
}
