import QtQuick 2.0
import Sailfish.Silica 1.0

import "../models"

Dialog {
    id: preferencesDialog

    property variant aQ: [1080, 720, 480, 360, 240]

    JSONListModel {
        id: bookmarksModel
        json: conf.bookmarks
     }

    SilicaFlickable {
        anchors.fill: parent
        contentHeight: column.height

        Column {
            id: column
            width: parent.width
            spacing: Theme.paddingMedium

            DialogHeader { }

            Label {
                text: qsTr("Video player")
                anchors.horizontalCenter: parent.horizontalCenter
                color: Theme.highlightColor
            }


            ComboBox {
                id:inputQuality
                width: parent.width
                label: qsTr("Preferred video quality")
                menu: ContextMenu {
                    MenuItem { text: "1080p" }
                    MenuItem { text: "720p" }
                    MenuItem { text: "480p" }
                    MenuItem { text: "360p" }
                    MenuItem { text: "240p" }
                }
            }

            TextSwitch {
                id: inputAutoPlay
                text: qsTr("Autoplay video")
            }

            ComboBox {
                id:inputDownloadQuality
                width: parent.width
                label: qsTr("Preferred video download quality")
                menu: ContextMenu {
                    MenuItem { text: "1080p" }
                    MenuItem { text: "720p" }
                    MenuItem { text: "480p" }
                    MenuItem { text: "360p" }
                    MenuItem { text: "240p" }
                }
            }


            Label {
                text: qsTr("Bookmarks")
                anchors.horizontalCenter: parent.horizontalCenter
                color: Theme.highlightColor
            }

            Button {
                anchors.horizontalCenter: parent.horizontalCenter
                text: qsTr("Reset bookmarks")
                onClicked:remorse.execute("Resetting", conf.resetBookmarks )

                RemorsePopup { id: remorse }
            }

            /*
            ComboBox {
                id: inputAutoopen
                width: parent.width
                label: qsTr("Instance to select on start")
                menu: ContextMenu {
                    MenuItem { text: qsTr("None") }
                    Repeater {
                        model: bookmarksModel.model
                        MenuItem { text: model.name }
                    }
                }
            }
            */


            Label {
                text: qsTr("Contents")
                anchors.horizontalCenter: parent.horizontalCenter
                color: Theme.highlightColor
            }

            TextSwitch {
                id: inputNSWF
                property bool hasBeenFound: false
                text: qsTr("Show NSWF in results")

                MouseArea {
                    anchors.fill: parent
                    preventStealing: true
                    propagateComposedEvents: true
                    onPressAndHold: {
                        console.log("press and hold");
                        parent.hasBeenFound = !parent.hasBeenFound
                    }
                }
            }

            TextSwitch {
                id: inputOnlyNSWF
                text: qsTr("Show Only NSWF")
                visible: checked || inputNSWF.hasBeenFound
            }

        }

        VerticalScrollDecorator {}
    }

    onOpened: {
        inputQuality.currentIndex = aQ.indexOf(conf.preferredQuality);
        inputDownloadQuality.currentIndex = aQ.indexOf(conf.preferredDownloadQuality);
        inputAutoPlay.checked = conf.autoPlay;
        //inputAutoopen.currentIndex = conf.autoopen + 1;
        inputNSWF.checked = conf.showNSFW;
        inputOnlyNSWF.checked = conf.showOnlyNSFW;
    }

    onDone: {
        if (result == DialogResult.Accepted) {
            conf.preferredQuality = aQ[inputQuality.currentIndex];
            conf.preferredDownloadQuality = aQ[inputDownloadQuality.currentIndex];
            conf.autoPlay = inputAutoPlay.checked;
            //conf.autoopen = inputAutoopen.currentIndex - 1;
            conf.showNSFW = inputNSWF.checked;
            // onlynsfw is true only if shownsfw is also true
            conf.showOnlyNSFW = inputOnlyNSWF.checked && inputNSWF.checked;
        }
    }
}
