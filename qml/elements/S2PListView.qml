import QtQuick 2.0
import Sailfish.Silica 1.0

import QtQuick 2.0
import Sailfish.Silica 1.0

SilicaListView {
    add: Transition {
        ParallelAnimation {
            NumberAnimation {
                property: "scale"
                from: 0.0
                to: 1.0
                duration: kStandardAnimationDuration
                easing.type: Easing.OutCubic
            }
            NumberAnimation {
                property: "height"
                from: 0
                duration: kStandardAnimationDuration
                easing.type: Easing.OutCubic
            }
            NumberAnimation {
                property: "opacity"
                from: 0.0
                to: 1.0
                duration: kStandardAnimationDuration
                easing.type: Easing.OutCubic
            }
        }
    }
    remove: Transition {
        ParallelAnimation {
            NumberAnimation {
                property: "scale"
                from: 1.0
                to: 0.0
                duration: kStandardAnimationDuration
                easing.type: Easing.OutCubic
            }
            NumberAnimation {
                property: "height"
                to: 0
                duration: kStandardAnimationDuration
                easing.type: Easing.OutCubic
            }
            NumberAnimation {
                property: "opacity"
                from: 1.0
                to: 0.0
                duration: kStandardAnimationDuration
                easing.type: Easing.OutCubic
            }
        }
    }
}

