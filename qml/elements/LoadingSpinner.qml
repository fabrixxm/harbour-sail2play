import QtQuick 2.0
import Sailfish.Silica 1.0

Item {
    id: loadingSpinner
    property Item model

    height:  100 + Theme.paddingLarge
    anchors {
        left: parent.left
        right: parent.right
        bottom: parent.bottom
        bottomMargin: -height
    }

    Rectangle {
        color: Theme.highlightBackgroundColor
        opacity: Theme.highlightBackgroundOpacity
        anchors.fill: parent;
    }

    BusyIndicator {
        id: indicator
        size: BusyIndicatorSize.Medium
        anchors.centerIn: parent
        running: false
    }


    states: State {
        name: "show";
        when: model.status === 0
        PropertyChanges { target: loadingSpinner; anchors.bottomMargin: 0  }
        PropertyChanges { target: indicator; running: true }
    }

    transitions: Transition {
        from: ""; to: "show"; reversible: true
        NumberAnimation { properties: "anchors.bottomMargin"; duration: kStandardAnimationDuration; easing.type: Easing.InOutQuad }
    }
}
