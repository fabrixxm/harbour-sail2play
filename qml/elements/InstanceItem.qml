import QtQuick 2.0
import Sailfish.Silica 1.0

ListItem {
    property bool simple: false
    id: delegate
    anchors {
        left: parent.left
        right: parent.right
    }
    contentHeight: column.height + (Theme.paddingSmall * 2)
    Column {
        id: column
        spacing: Theme.paddingSmall
        anchors {
            leftMargin: Theme.paddingLarge
            rightMargin: Theme.paddingLarge
            topMargin: Theme.paddingSmall
            left: parent.left
            right: parent.right
            top:parent.top
        }

        Label {
            text: model.name
            width: parent.width
            color: delegate.highlighted ? Theme.highlightColor : Theme.primaryColor
        }
        Label {
            text: "https://"+model.host
            width: parent.width
            color: delegate.highlighted ? Theme.secondaryHighlightColor : Theme.secondaryColor
            font.pixelSize: Theme.fontSizeTiny
        }
        Row {
            visible: !simple
            spacing: Theme.paddingSmall
            Image {
                source: "image://theme/icon-m-people?" + (delegate.highlighted ? Theme.secondaryHighlightColor : Theme.secondaryColor)
                sourceSize.width: Theme.iconSizeExtraSmall
                sourceSize.height: Theme.iconSizeExtraSmall
            }
            Label {
                text: model.totalUsers
                color: delegate.highlighted ? Theme.secondaryHighlightColor : Theme.secondaryColor
                font.pixelSize: Theme.fontSizeTiny
                width: parent.width / 4
            }

            Image {
                source: "image://theme/icon-m-video?" + (delegate.highlighted ? Theme.secondaryHighlightColor : Theme.secondaryColor)
                sourceSize.width: Theme.iconSizeExtraSmall
                sourceSize.height: Theme.iconSizeExtraSmall
            }
            Label {
                text: model.totalVideos
                color: delegate.highlighted ? Theme.secondaryHighlightColor : Theme.secondaryColor
                font.pixelSize: Theme.fontSizeTiny
                width: parent.width / 4
            }

            Image {
                source: "image://theme/icon-m-toy?" + (delegate.highlighted ? Theme.secondaryHighlightColor : Theme.secondaryColor)
                sourceSize.width: Theme.iconSizeExtraSmall
                sourceSize.height: Theme.iconSizeExtraSmall
            }
            Label {
                text: model.health + "%"
                color: delegate.highlighted ? Theme.secondaryHighlightColor : Theme.secondaryColor
                font.pixelSize: Theme.fontSizeTiny
                width: parent.width / 4
            }

        }


    }

}
