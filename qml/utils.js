.pragma library

function s2str(secs) {
    secs = Math.round(secs);
    var r = (secs%60);
    if (r<10) r = "0" + r;

    var mins = Math.floor(secs/60);
    if (mins>0) {
        r = (mins%60)+":"+r
        if ((mins%60)<10) r = "0" + r;
    } else {
        r = "00:" + r
    }

    var hours = Math.floor(mins/60);
    if (hours>0)
        r = (hours%60)+":"+r

    return r;
}

function json2date(str) {
    return new Date(Date.parse(str));
}
