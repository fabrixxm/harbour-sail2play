#include <QNetworkReply>
#include <QtDebug>

#include "proxy.h"

Proxy::Proxy(QTcpSocket *request, int id)
{
    qDebug() << "Proxy #" << id << " starting";
    this->request = request;
    this->id = id;
    connect(request, &QTcpSocket::readyRead, this, &Proxy::requestDataReady);
}

void Proxy::requestDataReady()
{
    qDebug() << "Proxy::requestDataReady()";

    if (reply == nullptr) {
        qDebug() << "No reply, new request. get headers and resend.";


        // original request headers
        QByteArray httpHeaders = request->readAll();
        httpHeaders = httpHeaders.replace("\r\n", "\n");

        // get http verb and url
        QByteArray req = httpHeaders.split('\n')[0];
        QByteArray verb = req.split(' ')[0].trimmed();
        QByteArray url = "https:/" + req.split(' ')[1].trimmed();


        const QUrl newUrl = QUrl::fromUserInput(url);
        if (!newUrl.isValid()) {
            request->write("HTTP/1.1 400 Bad Request\r\n\r\n");
            request->close();
            done();
            return;
        }


        // create a new request
        QNetworkRequest httpRequest(newUrl);

        // copy request headers
        httpHeaders = httpHeaders.mid(httpHeaders.indexOf('\n') + 1).trimmed();
        foreach(QByteArray line, httpHeaders.split('\n')) {
            int colon = line.indexOf(':');
            QByteArray headerName = line.left(colon).trimmed();
            QByteArray headerValue = line.mid(colon + 1).trimmed();
            // rewrite Host header
            if (headerName == "Host") {
                headerValue = newUrl.host().toLatin1(); // potrebbe dare problemi con host utf8?
            }
            qDebug() << " - " << headerName << " : " << headerValue;
            httpRequest.setRawHeader(headerName, headerValue);
        }


        // send the request
        reply = qnam.sendCustomRequest(httpRequest, verb);
        connect(reply, &QNetworkReply::finished, this, &Proxy::httpFinished);
        connect(reply, &QNetworkReply::metaDataChanged, this, &Proxy::httpMetadata);
        connect(reply, &QIODevice::readyRead, this, &Proxy::httpReadyRead);

        qDebug() << "Request " << newUrl.toString();
    }

}

void Proxy::httpMetadata()
{
    qDebug() << "Proxy::httpMetadata";
    QByteArray code = reply->attribute( QNetworkRequest::HttpStatusCodeAttribute ).toByteArray();
    QByteArray reason = reply->attribute( QNetworkRequest::HttpReasonPhraseAttribute ).toByteArray();

    qDebug() << "HTTP/1.1 " + code + " " + reason;

    request->write("HTTP/1.1 " + code + " " + reason + "\r\n");
    foreach (QByteArray headerName, reply->rawHeaderList()) {
        request->write(headerName+":"+reply->rawHeader(headerName)+"\r\n");
    }
    request->write("\r\n");

}

void Proxy::httpReadyRead()
{
    request->write(reply->readAll());
}

void Proxy::httpFinished()
{
    qDebug() << "Proxy::httpFinished";
    request->close();
    done();
}

void Proxy::sslErrors(QNetworkReply *, const QList<QSslError> &errors)
{
    qDebug() << "Proxy::sslErrors";
    request->write("HTTP/1.1 500 SSL Error\r\n\r\n");

    QString errorString;
    foreach (const QSslError &error, errors) {
        if (!errorString.isEmpty())
            errorString += '\n';
        errorString += error.errorString();
    }
    qDebug() << " -- " << errorString;

    request->write(errorString.toUtf8());
    request->close();
    done();
}

